
const validateName = (name) => {
  let message = null;
  if(name.length < 4 || name.length > 20) {
    message = `Required username length 4-20 symbols`;
  } else {
    message = null;
  }

  return message;
} 

const validatePassword = (pass) => {
  let message = null;
  if(pass.length < 6 || pass.length > 20) {
    message = `Required password length 6-20 symbols`;
  } else {
    message = null;
  }

  return message;
}

export {
  validateName,
  validatePassword
}