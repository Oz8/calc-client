import { useEffect, useState } from 'react';
import { useRouter } from "next/router";

const Header = () => {
  const route = useRouter();

  return(
    <header className="header">
    <div className="header-cover">
      <h1>Calculator</h1>
    </div>  
  </header>
  )
}

export default Header;