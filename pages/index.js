import { useEffect, useState } from "react";
import { useRouter } from "next/router";

const Home = () => {
  const route = useRouter();
  const [ calculations, setCalculations ] = useState(null);
  const [ number, setNumber ] = useState('');
  const [ equation, setEquation ] = useState('');
  const [ result, setResult ] = useState('0');

  const fetchData = async () => {
    const id = localStorage.getItem('userID');
    const formData = new URLSearchParams({userID: id});

    const response = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/get-calculations`,
      {
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        body: formData,
        method: "post"
      });

    const data = await response.json();
    setCalculations(data);
  }

  useEffect(()=> {
    const userID = localStorage.getItem('userID');

    if(!userID) {
      route.push('/login');
    } else {
      fetchData();
    }
  }, []);

  const addCalculation = async (userID, equation, result) => {
    const sendData = new URLSearchParams({userID, equation, result});
  
    fetch(`${process.env.NEXT_PUBLIC_API_HOST}/add-calculation`,
      {
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        body: sendData,
        method: 'post',
      })
      .then((res) => res.json())
      .then((resJSON) => {
        if(resJSON.errorMessage) {
          console.log(resJSON.errorMessage);
        } else {
          fetchData();
        }
      })
      .catch((error) => console.log(error));
  }


  // const addNumber = (e) => {
  //   let newValue;
  //   let value = e.target.dataset.val.toString();
  //   newValue = parseInt(value, 10);
  //   // if(equation.charAt(0) === '0') {
  //   //   newValue = value.slice(1);
  //   //   setEquation(equation.slice(1));
  //   // }
  //   newValue = number + value;
  //   setNumber(newValue);
  //   addToEquation();
  //   setResult(newValue);
  // }

  // const addSign = (e) => {
  //   let newValue;
  //   let value = e.target.dataset.val.toString();
  //   newValue = parseInt(value, 10);
  //   // if(equation.charAt(0) === '0') {
  //   //   newValue = value.slice(1);
  //   //   setEquation(equation.slice(1));
  //   // }
  //   newValue = number + value;
  //   setEquation(newValue);
  //   setResult(newValue);
  // }


  const addToEquation = (e) => {
    let newValue;
    let value = e.target.dataset.val.toString();
    // newValue = parseInt(equation, 10);
    // console.log(newValue);
    if(equation.length > 0 || value != '0') {

      newValue = equation + value;
      setEquation(newValue);
      setResult(newValue);
    }
    // console.log('anewValue : ', anewValue);
    // if(equation.charAt(0) === '0') {
    //   newValue = value.slice(1);
    //   setEquation(equation.slice(1));
    // }
  }

  const getResult = () => {
    if(equation) {
      const geval = eval;
      try {
        geval(equation);
        let result = geval(equation);

        if(result) {
          setResult(result);
          updateData(equation, result);
        } else {
          clearResult();
        }
      } catch (e) {
        if (e instanceof SyntaxError) {
          setResult('Malformed expression');
          setEquation('');
        }
      }
    }
  }

  const updateData = (equation, result) => {
    const userID = localStorage.getItem('userID');
    addCalculation(userID, equation, result);    
  }

  const clearResult = () => {
    setResult('0');
    setEquation('');
  }

  return (
    <>
      <div className="calc-body">
        <div className="calc-screen">{ result }</div>
        <div className="digits">
          <button className="digit-button" data-val="9" onClick={ addToEquation }>9</button>
          <button className="digit-button" data-val="8" onClick={ addToEquation }>8</button>
          <button className="digit-button" data-val="7" onClick={ addToEquation }>7</button>
          <button className="digit-button" data-val="6" onClick={ addToEquation }>6</button>
          <button className="digit-button" data-val="5" onClick={ addToEquation }>5</button>
          <button className="digit-button" data-val="4" onClick={ addToEquation }>4</button>
          <button className="digit-button" data-val="3" onClick={ addToEquation }>3</button>
          <button className="digit-button" data-val="2" onClick={ addToEquation }>2</button>
          <button className="digit-button" data-val="1" onClick={ addToEquation }>1</button>
          <button className="sign-button" data-val="." onClick={ addToEquation }>.</button>
          <button className="digit-button zero-btn" data-val="0" onClick={ addToEquation }>0</button>
        </div>
        <div className="signs">
          <button className="sign-button" data-val="+" onClick={ addToEquation }>+</button>
          <button className="sign-button" data-val="-" onClick={ addToEquation }>-</button>
          <button className="sign-button" data-val="*" onClick={ addToEquation }>*</button>
          <button className="sign-button" data-val="/" onClick={ addToEquation }>/</button>
        </div>
        <div className="controlls">
          <button className="ac-button" onClick={ clearResult }>AC</button>
          <button className="equal-button" onClick={ getResult }>=</button>
        </div>
      </div>
      <ul className="results-list">
        {calculations && calculations.map(({ _id, equation, result }) => (
          <li key={ _id }>{ equation } = { result }</li>
        ))}
      </ul>
    </>
  )
};

export default Home;