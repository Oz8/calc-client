import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from 'next/link';
import { validateName, validatePassword } from '../../services/form-validate';

const Login = () => {

  const route = useRouter();
  const [ userNameInput, setUserNameInput ] = useState('');
  const [ passwordInput, setPasswordInput ] = useState('');
  const [ serverErrorResponse, setServerErrorResponse ] = useState(null);
  const [ errorUserName, setErrorUserName ] = useState(null);
  const [ errorPassword, setErrorPassword ] = useState(null);
  const [ formValidation, setFormValidation ] = useState(false);

  useEffect(()=> {
    fetch(`${process.env.NEXT_PUBLIC_API_HOST}/get-user`,
      {
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials: true,
        method: 'get',
        credentials: 'include'
      })
      // .then((response) => {
  
      //   for(let entry of response.headers.entries()) {
      //     console.log('header', entry);
      //   }})
      .then((res) => res.json())
      .then((resJSON) => {
        if(resJSON.errorMessage) {
          setServerErrorResponse(resJSON.errorMessage);
        } else if(resJSON.logged) {
          console.log("resJSON true?? ", resJSON);
          
          // localStorage.setItem('userID', resJSON);
          // route.push('/');
        } else {
          console.log("resJSON false?? ", resJSON);
        };
      })
      .catch((error) => console.log(error));

    // if(userID) {
    //   route.push('/');
    // }
  }, []);

  const submitForm = (e) => {
    e.preventDefault();

    setErrorUserName(validateName(userNameInput));
    setErrorPassword(validatePassword(passwordInput));

    if(!errorUserName && !errorPassword) {
      setFormValidation(true);
    }

    if(formValidation) {
      const formData = new URLSearchParams({name: userNameInput, password: passwordInput});

      fetch(`${process.env.NEXT_PUBLIC_API_HOST}/login`,
        {
          headers: { 'content-type': 'application/x-www-form-urlencoded' },
          body: formData,
          method: 'post',
        })
        .then((res) => res.json())
        .then((resJSON) => {
          if(resJSON.errorMessage) {
            setServerErrorResponse(resJSON.errorMessage);
          } else {
            // localStorage.setItem('userID', resJSON);
            // route.push('/');
            console.log('login response  : ', resJSON);
            console.log('login response  : ', resJSON);
          }
        })
        .catch((error) => console.log(error));
    }
  }

  const logout = () => {
    fetch(`${process.env.NEXT_PUBLIC_API_HOST}/logout`,
      {
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        method: 'post',
      })
      .then((res) => res.json())
      .then((resJSON) => {
        if(resJSON.errorMessage) {
          setServerErrorResponse(resJSON.errorMessage);
        } else {
          console.log("Logged out?? ", resJSON);
        }
      })
      .catch((error) => console.log(error));

  }

  return(
    <div className="form-cover">
      <h1>Login</h1>
      <form action="#" method="post" onSubmit={ submitForm }>
        <input type="text" placeholder="username" value={ userNameInput } onChange = {(e) => setUserNameInput(e.target.value)} />
        <p className="error">{ errorUserName }</p>
        <input type="password" placeholder="password" value={ passwordInput } onChange = {(e) => setPasswordInput(e.target.value)} />
        <p className="error">{ errorPassword || serverErrorResponse }</p>
        <input type="submit" />
      </form>
      <Link className="link" href="/signup">Create an account</Link>
      <button onClick={ logout } >Logout</button>
    </div>
  )
}

export default Login;