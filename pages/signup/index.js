import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { validateName, validatePassword } from '../../services/form-validate';

const Signup = () => {

  const route = useRouter();
  const [ userNameInput, setUserNameInput ] = useState('');
  const [ passwordInput, setPasswordInput ] = useState('');
  const [ serverErrorResponse, setServerErrorResponse ] = useState(null);
  const [ errorUserName, setErrorUserName ] = useState(null);
  const [ errorPassword, setErrorPassword ] = useState(null);
  const [ formValidation, setFormValidation] = useState(false);

  useEffect(()=> {
    const userID = localStorage.getItem('userID');

    if(userID) {
      route.push('/');
    } 
  }, []);

  const submitForm = (e) => {
    e.preventDefault();

    setErrorUserName(validateName(userNameInput));
    setErrorPassword(validatePassword(passwordInput));

    if(!errorUserName && !errorPassword) {
      setFormValidation(true);
    }

    if(formValidation) {
      const formData = new URLSearchParams({name: userNameInput, password: passwordInput});

      fetch(`${process.env.NEXT_PUBLIC_API_HOST}/sign-up`,
        {
          headers: { 'content-type': 'application/x-www-form-urlencoded' },
          body: formData,
          method: 'post',
        })
        .then((res) => res.json())
        .then((resJSON) => {
          if(resJSON.errorMessage) {
            setServerErrorResponse(resJSON.errorMessage);
          } else {
            localStorage.setItem('userID', resJSON);
            route.push('/');
          }
        })
        .catch((error) => console.log(error));
    }
  }

  return (
    <div className="form-cover">
      <h1>Signup</h1>
      <form action="#" method="post" onSubmit={ submitForm }>
        <input type="text" placeholder="username" value={ userNameInput } onChange = {(e) => setUserNameInput(e.target.value)} />
        <p className="error">{ errorUserName }</p>
        <input type="password" placeholder="password" value={ passwordInput } onChange = {(e) => setPasswordInput(e.target.value)} />
        <p className="error">{ errorPassword || serverErrorResponse }</p>
        <input type="submit" value="Submit" />
      </form>
      <Link className="link" href="/login">Login</Link>
    </div>
  )
};

export default Signup;