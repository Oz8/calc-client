import Layout from '../components/Layout';
import "@/styles/globals.css";
import '@/styles/Header.css';
import "@/styles/Home.css";
import '@/styles/Forms.css';

const App = ({ Component, pageProps }) => (
  <Layout>
    <Component {...pageProps} />
  </Layout>
)

export default App;